import { moduleFor, test } from 'ember-qunit';

moduleFor('route:index', 'Unit | Route | index');

test('redirects to dashboard', function(assert) {
  let route = this.subject({
    replaceWith(routeName) {
      assert.equal(routeName, 'dashboard', 'replace with route dashboard')
    }
  });

  route.beforeModel();
});
